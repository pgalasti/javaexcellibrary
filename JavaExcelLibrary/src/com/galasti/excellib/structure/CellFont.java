package com.galasti.excellib.structure;

/**
 * Structure to represent a cell text font.
 * @author pgalasti
 *
 */
public class CellFont {

	private String name;
	private long size;
	
	private static String DEFAULT_FONT = "Calibri";
	private static long DEFAULT_SIZE = 12;
	
	/**
	 * Creates a CellFont object with default font of size 12 point and default
	 * font of Calibri.
	 */
	public CellFont() {
		this.setFont(DEFAULT_FONT);
		this.setSize(DEFAULT_SIZE);
	}
	
	/**
	 * Creates a CellFont object.
	 * @param name The name of the font
	 * @param size The size of the font point
	 */
	public CellFont(String name, long size) {
		this.setFont(name);
		this.setSize(size);
	}
	
	/**
	 * Sets the size of the font. Must be greater than 0.
	 * @param size The size of the font
	 */
	public void setSize(long size) {
		if(size < 1) {
			throw new RuntimeException("Font size set less than 1!");
		}
		
		this.size = size;
	}
	
	/**
	 * Returns the size font point.
	 * @return The size font point
	 */
	public long getSize() {
		return this.size;
	}
	
	/**
	 * Sets the font name.
	 * @param name The font name
	 */
	public void setFont(String name) {
		this.name = name;
	}
	
	/**
	 * Returns the font name.
	 * @return The font name
	 */
	public String getFont() {
		return this.name;
	}
	
	
	
}
