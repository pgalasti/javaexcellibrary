package com.galasti.excellib.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents a workbook document as an object. A workbook contains an order of
 * spreadsheets.
 * @author pgalasti
 *
 */
public class WorkbookDocument {

	protected Map<String, SpreadSheet> sheets;
	protected Map<Integer, String> sheetOrder;
	
	protected String name;
	
	/**
	 * Creates an unnamed blank workbook object.
	 */
	public WorkbookDocument() {
		this.setName("");
		this.sheetOrder = new HashMap<>();
		this.sheets = new HashMap<>();
	}
	
	/**
	 * Creates a blank workbook object.
	 * @param name Name of the workbook.
	 */
	public WorkbookDocument(String name) {
		this();
		this.setName(name);
	}
	
	/**
	 * Sets the name of the workbook.
	 * @param name Name of the workbook
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Returns the name of the workbook.
	 * @return Name of the workbook
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Returns a {@link SpreadSheet} object based on the name. If the workbook
	 * doesn't contain the SpreadSheet object, it will return null.
	 * @param name The name property of the {@link SpreadSheet} object.
	 * @return A {@link SpreadSheet} if found. Null if not.
	 */
	public SpreadSheet getSheet(String name) {
		// Get the lower case name so the map can match it correctly
		final String sheetName = name.toLowerCase();
		
		return this.sheets.get(sheetName);
	}
	
	/**
	 * Returns a {@link SpreadSheet} object based on the sheet order starting 
	 * with index 1. Order cannot be less than 1. If the workbook doesn't 
	 * contain a Spreadsheet at the number indicated, it will return null.
	 * @param number The number order of the {@link SpreadSheet} object
	 * @return A {@link SpreadSheet} if found. Null if not.
	 */
	public SpreadSheet getSheet(int number) {
		
		if(number < 1) {
			throw new RuntimeException("Cannot get sheet! Sheet value must be greater than 0!");
		}
		
		// Find the sheet order first
		final String sheetName = this.sheetOrder.get(number);
		
		// Find the sheet from the found order
		return this.getSheet(sheetName);
	}
	
	/**
	 * Adds a {@link SpreadSheet} to the workbook that is next in order.
	 * A RunTimeException will be thrown if a SpreadSheet with the same
	 * name property already exists.
	 * @param sheet A {@link SpreadSheet}
	 */
	public void addSheet(SpreadSheet sheet) {
		// Get the lower case name so the map can match it correctly
		final String sheetName = sheet.getName().toLowerCase();
		
		// Make sure there isn't already a sheet with the same name
		if(this.sheets.containsKey(sheetName)) {
			throw new RuntimeException("Cannot add sheet! A sheet with the name of " + name + " already exists!");
		}
		
		this.sheets.put(sheetName, sheet);
		
		// Get the index of the next sheet position
		final int thisSheetPosition = this.sheets.size();
		
		this.sheetOrder.put(thisSheetPosition, sheetName);
	}
	
	/**
	 * Returns a {@link List} of {@link SpreadSheet} objects in the workbook.
	 * @return A {@link List} of {@link SpreadSheet} objects
	 */
	public List<SpreadSheet> getSheets() {
		
		List<SpreadSheet> sheetList = new ArrayList<>();
		final int numberOfSheets = sheetOrder.size();
		
		for(int i = 1; i <= numberOfSheets; i++) {
			sheetList.add(this.getSheet(i));
		}
		
		return sheetList;
	}
	
}
