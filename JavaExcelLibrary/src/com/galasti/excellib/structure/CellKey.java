package com.galasti.excellib.structure;

/**
 * Represents a unique row/column position on an excel sheet for a cell.<br/>
 * Columns are represented with letters. Rows are represented with whole 
 * numbers.
 * @author pgalasti
 *
 */
public class CellKey {

	private final static String COLUMN_REGEX = "[a-zA-Z]+"; // Change this regex to match 
	
	protected String column;
	protected int row;
	
	/**
	 * Creates a new CellKey object without a column/row position.
	 */
	public CellKey() {
		this.column = "";
		this.row = 0;
	}
	
	/**
	 * Creates a new CellKey object with a column/row position.
	 * @param column The letter column of the cell key
	 * @param row The row of the cell
	 */
	public CellKey(String column, int row) {
		this.column = column;
		this.row = row;
	}
	
	/**
	 * Creates a new CellKey object with a column/row position. The column 
	 * parameter is converted to the appropriate letter value.
	 * @param column The integer column of the cell key
	 * @param row The row of the cell
	 */
	public CellKey(int column, int row) {
		this.column = CellKey.getNumericColumnString(column);
		this.row = row;
	}
	
	/**
	 * Sets the column of the CellKey object. The column must be a valid column
	 * letter value.
	 * @param column The letter column of the cell key.
	 */
	public void setColumn(String column) {
		
		if(!column.matches(COLUMN_REGEX)) { 
			throw new RuntimeException("Invalid Key! Cell column value must contain a letters only!");
		}
		
		this.column = column;
	}
	
	/**
	 * Returns the letter column of the cell key
	 * @return The letter column.
	 */
	public String getColumn() {
		return this.column;
	}
	
	/**
	 * Sets the row of the cell key. The row must be greater than 0.
	 * @param row The row of the cell key.
	 */
	public void setRow(int row) {
		
		if(row < 1) {
			throw new RuntimeException("Invalid Key! Cell row value must be greater than 0!");
		}
		
		this.row = row;
	}
	
	/**
	 * Returns the row of the cell key.
	 * @return The row of the cell key
	 */
	public int getRow() {
		return this.row;
	}
	
	/**
	 * Returns the cell key in <column><row> format as Excel would reference.
	 * @return The string representation of the CellKey object
	 */
	@Override
	public String toString() {
		return new StringBuffer(column).append(row).toString();
	}
	
	/**
	 * Returns a unique hash code for the String representation of the CellKey
	 * object.
	 */
	@Override
	public int hashCode() {
		return this.toString().hashCode(); // Should return unique hash code... I hope
	}
	
	@Override
	public boolean equals(Object object) {

		if(object instanceof CellKey) {
			CellKey otherCellKey = ((CellKey) object);
			return (otherCellKey.toString() == this.toString());

		}
		return false;
	}
	
	private static int ASCII_VALUE_START_INDEX = 65;
	private static int SECOND_CHARACTER_START_INDEX = 26;
	private static int THIRD_CHARACTER_START_INDEX = 702;
	private static int FIRST_CHARACTER_SET_LENGTH = 676;
	
	/**
	 * Returns the equivalent column numeric value.
	 * @return Numeric column
	 */
	public int getColumnNumber() {
		
		final String upperColumn = this.column.toUpperCase();
		int value = 0;
		
		// There is a more elegant way to do this with recursion I believe. 
		// Since an excel sheet will not accept values beyond a certain 3 
		// character column value, this method may be better at performance
		// then the recursive solution. Paul
		if(upperColumn.length() == 1) {
			final int firstCharValue = getCharacterValue(upperColumn.charAt(0));
			
			value = firstCharValue;
		} else if(upperColumn.length() == 2) {
			final int firstCharValue = getCharacterValue(upperColumn.charAt(0));
			final int secondCharValue = getCharacterValue(upperColumn.charAt(1));
			
			value = (SECOND_CHARACTER_START_INDEX * (firstCharValue+1)) + secondCharValue;
		} else if(upperColumn.length() == 3) {
			final int firstCharValue = getCharacterValue(upperColumn.charAt(0));
			final int secondCharValue = getCharacterValue(upperColumn.charAt(1));
			final int thirdCharValue = getCharacterValue(upperColumn.charAt(2));
			
			// First character Index
			value = THIRD_CHARACTER_START_INDEX + (FIRST_CHARACTER_SET_LENGTH*firstCharValue);
			
			// Second character Index
			value += (SECOND_CHARACTER_START_INDEX*(secondCharValue));
			
			// Third character value
			value += thirdCharValue;
		}
		
		return value;
	}
	
	/**
	 * Returns the numeric value of a column character
	 * @param character A column character
	 * @return The numeric value of that character.
	 */
	public static int getCharacterValue(char character) {
		final int characterColumnValue = ((int)character-ASCII_VALUE_START_INDEX);
		return characterColumnValue;
	}
	
	/**
	 * Returns a column letter string based on the numeric parameter.
	 * @param number The numeric column
	 * @return The column letter string.
	 */
	public static String getNumericColumnString(int number){
        String converted = "";
        while (number >= 0)
        {
            int remainder = number % 26;
            converted = (char)(remainder + 'A') + converted;
            number = (number / 26) - 1;
        }

        return converted;
    }
}
