package com.galasti.excellib.structure;

/**
 * Represents the data types of cells in an Excel sheet.
 * @author pgalasti
 *
 */
public enum DataType {

	/**
	 * Represents a whole number data type.
	 */
	WholeNumber,
	
	/**
	 * Represents a decimal data type.
	 */
	Decimal,
	
	/**
	 * Represents a string data type.
	 */
	Text,
//	Function?
}
