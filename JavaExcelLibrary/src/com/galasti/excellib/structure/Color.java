package com.galasti.excellib.structure;

/**
 * Represents 24 bit color.
 * @author pgalasti
 *
 */
public class Color {

	private byte red;
	private byte green;
	private byte blue;
	
	/**
	 * Returns a Color object with 0x000000 color.
	 */
	public Color() {
		this.red = this.green = this.blue = 0;
	}
	
	/**
	 * Returns a color object.
	 * @param red Byte value of red 
	 * @param green Byte value of green
	 * @param blue Byte value of blue
	 */
	public Color(byte red, byte green, byte blue) {
			this.red = red;
			this.green = green;
			this.blue = blue;
	}
	
	/**
	 * Checks if Color object is complete black.
	 * @return Whether color object is complete black.
	 */
	public boolean isBlack() {
		return (this.getRed() == 255 && this.getGreen()== 255 && this.getBlue()== 255);
	}
	
	/**
	 * Checks if Color object is complete white.
	 * @return Whether color object is complete white.
	 */
	public boolean isWhite() {
		return (this.getRed() == 0 && this.getGreen()== 0 && this.getBlue()== 0);
	}
	
	/**
	 * Checks if there is any bit value of red.
	 * @return Whether color object has red value.
	 */
	public boolean hasRed() {
		return this.red > 0;
	}
	
	/**
	 * Checks if there is any bit value of green.
	 * @return Whether color object has green value.
	 */
	public boolean hasGreen() {
		return this.green > 0;
	}
	
	/**
	 * Checks if there is any bit value of blue.
	 * @return Whether color object has blue value.
	 */
	public boolean hasBlue() {
		return this.blue > 0;
	}
	
	/**
	 * Returns the red byte value to unsigned integer.
	 * @return The red byte value to unsigned integer.
	 */
	public int getRed() {
		return Byte.toUnsignedInt(this.red);
	}
	
	/**
	 * Returns the green byte value to unsigned integer.
	 * @return The green byte value to unsigned integer.
	 */
	public int getGreen() {
		return Byte.toUnsignedInt(this.green);
	}
	
	/**
	 * Returns the blue byte value to unsigned integer.
	 * @return The blue byte value to unsigned integer.
	 */
	public int getBlue() {
		return Byte.toUnsignedInt(this.blue);
	}
}
