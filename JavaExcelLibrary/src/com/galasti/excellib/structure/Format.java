package com.galasti.excellib.structure;

/**
 * Structure containing any cell formatting. Setting formats follow the builder
 * pattern to set up formatting easier.
 * @author pgalasti
 *
 */
public class Format {

	protected Color textColor;
	protected Color backgroundColor;
	protected DataType dataType;
	protected TextType textType;
	protected CellFont font;
	
	/**
	 * Creates a new Format object with default cell format.
	 */
	public Format() {
		this.textColor = new Color();
		this.backgroundColor = new Color((byte)-1, (byte)-1, (byte)-1);
		this.textType = new TextType();
		dataType = DataType.Text;
		this.font = new CellFont();
	}
	
	/**
	 * Sets a {@link Color} object for the text.
	 * @param color A {@link Color} object
	 * @return This Format object
	 */
	public Format setTextColor(Color color) {
		this.textColor = color;
		return this;
	}

	/**
	 * Returns a {@link Color} object for the text.
	 * @return The {@link Color} object
	 */
	public Color getTextColor() {
		return this.textColor;
	}
	
	/**
	 * Sets a {@link Color} object for the background.
	 * @param color A {@link Color} object
	 * @return This Format object
	 */
	public Format setBackgroundColor(Color color) {
		this.backgroundColor = color;
		return this;
	}
	
	/**
	 * Returns a {@link Color} object for the background.
	 * @return A {@link Color} object
	 */
	public Color getBackgroundColor() {
		return this.backgroundColor;
	}
	
	/**
	 * Sets the {@link DataType} enumeration for the object.
	 * @param type A {@link DataType} enumeration.
	 * @return This Format object
	 */
	public Format setDataType(DataType type) {
		this.dataType = type;
		return this;
	}
	
	/**
	 * Returns a {@link DataType} enumeration for the object.
	 * @return A {@link DataType} enumeration
	 */
	public DataType getDataType() {
		return this.dataType;
	}
	
	/**
	 * Sets the {@link TextType} object for the text.
	 * @param type A {@link TextType} object
	 * @return This Format object
	 */
	public Format setTextType(TextType type) {
		this.textType = type;
		return this;
	}
	
	/**
	 * Returns the {@link TextType} object.
	 * @return A {@link TextType} object
	 */
	public TextType getTextType() {
		return this.textType;
	}
	
	/**
	 * Sets the {@link CellFont} object for the font.
	 * @param font A {@link CellFont} object
	 * @return This Format object
	 */
	public Format setFont(CellFont font) {
		this.font = font;
		return this;
	}
	
	/**
	 * Returns a {@link CellFont} object for the font.
	 * @return A {@link CellFont} object
	 */
	public CellFont getFont() {
		return this.font;
	}
}
