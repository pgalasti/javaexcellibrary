package com.galasti.excellib.structure;

/**
 * Represents an Excel cell containing position, format, and data.
 * @author pgalasti
 *
 */
public class Cell {

	protected CellKey cellKey;
	protected Format format;
	protected String data;
	
	/**
	 * Creates a new Cell object with no position, format, or data. 
	 */
	public Cell() {
		this.cellKey = new CellKey();
		this.format = new Format();
		this.data = "";
	}
	
	/**
	 * Creates a new Cell object with no format or data.
	 * @param key The {@link CellKey} of the cell
	 */
	public Cell(CellKey key) {
		this();
		this.setKey(key);
	}
	
	/**
	 * Creates a new Cell object with no format.
	 * @param key The {@link CellKey} of the cell
	 * @param data The data of the cell
	 */
	public Cell(CellKey key, String data) {
		this(key);
		this.data = data;
	}
	
	/**
	 * Creates a new Cell object.
	 * @param key The {@link CellKey} of the cell
	 * @param data The data of the cell
	 * @param format The {@link Format} of the cell
	 */
	public Cell(CellKey key, String data, Format format) {
		this(key, data);
		this.setFormat(format);
	}
	
	/**
	 * Sets the column and row of the cell {@link CellKey}.
	 * @param column Column of the cell
	 * @param row Row of the cell
	 */
	public void setKey(String column, int row) {
		this.setKey(new CellKey(column, row));
	}
	
	/**
	 * Sets the {@link CellKey} of the cell.
	 * @param key The {@link CellKey} of the cell
	 */
	public void setKey(CellKey key) {
		this.cellKey.setColumn(key.getColumn());
		this.cellKey.setRow(key.getRow());
	}
	
	/**
	 * Returns the {@link CellKey} of the cell.
	 * @return The cell {@link CellKey}
	 */
	public CellKey getCellKey() {
		return this.cellKey;
	}
	
	/**
	 * Sets the {@link Format} of the cell.
	 * @param format The {@link Format} of the cell
	 */
	public void setFormat(Format format) {
		this.format = format;
	}
	
	/**
	 * Returns the {@link Format} of the cell.
	 * @return The {@link Format} of the cell
	 */
	public Format getFormat() {
		return this.format;
	}
	
	/**
	 * Sets the data of the cell.
	 * @param data The data of the cell
	 */
	public void setData(String data) {
		this.data = data;
	}
	
	/**
	 * Returns the data of the cell.
	 * @return The data of the cell.
	 */
	public String getData() {
		return this.data;
	}
	
	/**
	 * Returns the column number representation of the cell.
	 * @return The column number of the cell.
	 */
	public int getColumnNumber() {
		return this.cellKey.getColumnNumber();
	}
	
	@Override
	public int hashCode() {
		return this.getCellKey().hashCode(); // Should return unique hash code... I hope
	}
	
	@Override
	public boolean equals(Object object) {

		if(object instanceof Cell) {
			Cell otherCell = ((Cell) object);
			return (this.getCellKey().equals(otherCell.getCellKey()) &&
					this.data == otherCell.getData());
		}
		return false;
	}
	
}
