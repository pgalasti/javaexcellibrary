package com.galasti.excellib.structure;

/**
 * Structure to represent the text type format of a cell.
 * @author pgalasti
 *
 */
public class TextType {

	protected long textTypeBits;
	
	/**
	 * No Text Setting
	 */
	public static long BLANK 		= 0x0000;
	/**
	 * Italic Text
	 */
	public static long ITALIC 		= 0x0002;
	/**
	 * Bold Text
	 */
	public static long BOLD 		= 0x0004;
	/**
	 * Underline Text
	 */
	public static long UNDERLINE 	= 0x0008;
	
	/**
	 * Creates a TextType object set as blank format.
	 */
	public TextType() {
		this.setTypeBits(BLANK);
	}
	
	/**
	 * Creates a TextType object with bit settings.
	 * @param settings Bit settings
	 */
	public TextType(long settings) {
		this.setTypeBits(settings);
	}
	
	/**
	 * Sets the bit settings of the object.
	 * @param settings Bit settings
	 */
	public void setTypeBits(long settings) {
		this.textTypeBits = settings;
	}
	
	/**
	 * Sets the object as italic.
	 * @return This TextType object
	 */
	public TextType setItalic() {
		this.textTypeBits |= ITALIC;
		return this;
	}
	
	/**
	 * Unsets the object as italic.
	 * @return This TextType object
	 */
	public TextType unSetItalic() {
		this.textTypeBits &= ITALIC;
		return this;
	}
	
	/**
	 * Sets the object as bold.
	 * @return This TextType object
	 */
	public TextType setBold() {
		this.textTypeBits |= BOLD;
		return this;
	}

	/**
	 * Unsets the object as bold.
	 * @return This TextType object
	 */
	public TextType unSetBold() {
		this.textTypeBits &= BOLD;
		return this;
	}
	
	/**
	 * Sets the object as underline.
	 * @return This TextType object
	 */
	public TextType setUnderline() {
		this.textTypeBits |= UNDERLINE;
		return this;
	}
	
	/**
	 * Unsets the object as underline.
	 * @return This TextType object
	 */
	public TextType unSetUnderline() {
		this.textTypeBits &= UNDERLINE;
		return this;
	}
	
	/**
	 * Checks if the object format is italic.
	 * @return Whether the object is italic
	 */
	public boolean isItalic() {
		return (this.textTypeBits & ITALIC) == ITALIC;
	}

	/**
	 * Checks if the object format is bold.
	 * @return Whether the object is bold
	 */
	public boolean isBold() {
		return (this.textTypeBits & BOLD) == BOLD;
	}
	
	/**
	 * Checks if the object format is underline.
	 * @return Whether the object is underline
	 */
	public boolean isUnderline() {
		return (this.textTypeBits & UNDERLINE) == UNDERLINE;
	}
}
