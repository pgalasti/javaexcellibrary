package com.galasti.excellib.structure;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents a spreadsheet as an object. A spreadsheet contains a name and
 * cells.
 * @author pgalasti
 *
 */
public class SpreadSheet {

	protected String name;
	protected Map<CellKey, Cell> cells;
	
	/**
	 * Creates a new unnamed SpreadSheet object.
	 */
	public SpreadSheet() {
		this.setName("");
		cells = new HashMap<>();
	}
	
	/**
	 * Creates a new named SpreadSheet object.
	 * @param name Name of the spreadsheet
	 */
	public SpreadSheet(String name) {
		this();
		this.setName(name);
	}
	
	/**
	 * Sets the name of the SpreadSheet object.
	 * @param name The name of the spreadsheet
	 */
	public void setName(String name) { // TODO research if sheet name has restricted characters
		this.name = name;
	}
	
	/**
	 * Returns the name of the spreadsheet object.
	 * @return The spreadsheet name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Returns a {@link Cell} object based on a {@link CellKey}.
	 * @param key A {@link CellKey} object
	 * @return A {@link Cell} object if found. Null if not
	 */
	public Cell getCell(CellKey key) {
		return this.cells.get(key);
	}
	
	/**
	 * Returns a {@link Cell} object based on a column/row.
	 * @param column The column of the {@link Cell}
	 * @param row The row of the {@link Cell}
	 * @return A {@link Cell} object if found. Null if not
	 */
	public Cell getCell(String column, int row) {
		return this.cells.get(new CellKey(column, row));
	}
	
	/**
	 * Adds a {@link Cell} object to the sheet.
	 * @param cell A {@link Cell} object
	 */
	public void addCell(Cell cell) {
		this.cells.put(cell.getCellKey(), cell);
	}
	
	/**
	 * Returns a {@link Collection} of {@link Cell} objects in the spreadsheet.
	 * @return A {@link Collection} of {@link Cell} objects
	 */
	public Collection<Cell> getCells() {
		return this.cells.values();
	}
	
	@Override
	public int hashCode() {
		return this.getName().hashCode(); // Should return unique hash code... I hope
	}
	
	@Override
	public boolean equals(Object object) {

		if(object instanceof SpreadSheet) {
			SpreadSheet otherSheet = ((SpreadSheet) object);
			return (otherSheet.getName() == this.getName());

		}
		return false;
	}
}
