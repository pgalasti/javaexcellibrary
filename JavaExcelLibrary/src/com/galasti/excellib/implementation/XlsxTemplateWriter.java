package com.galasti.excellib.implementation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.galasti.excellib.entry.IWorkbookTemplateWriter;
import com.galasti.excellib.structure.Cell;
import com.galasti.excellib.structure.SpreadSheet;

/**
 * Writes onto an existing Excel xlsx type file from a {@link List} of
 * {@link SpreadSheet} objects. The current xlsx file must exist to write onto.
 * <br/><br/>
 * The purpose of using this class vs the XlsXWriter class is to get around 
 * the 1804/1810 validations/macros that aren't platform independent for 
 * spreadsheet framework.
 * @author pgalasti
 *
 */
public class XlsxTemplateWriter implements IWorkbookTemplateWriter{

	private XSSFWorkbook workbook;
	private RowManager rowManager;
	private File templateFile;
	
	/**
	 * Creates a new XlsxTemplateWriter object
	 */
	public XlsxTemplateWriter() {
		this.rowManager = new RowManager();
	}
	
	@Override
	public void put(List<SpreadSheet> sheets) {
		if(this.workbook == null || !templateFile.exists()) {
			throw new RuntimeException("Workbook needs to be loaded. Call load() first.");
		}
		
		// Validate they're all there
		this.performSheetValidation(sheets);

		// For each sheet, write onto the template sheet
		for(SpreadSheet sheet : sheets) {
			this.applySheet(sheet);
		}
		
		FileOutputStream fileOut;
		try {
			fileOut = new FileOutputStream(this.templateFile);
			this.workbook.write(fileOut);
			
		} catch (IOException e) {
			throw new RuntimeException("Unable to save template with changes!", e);
		}
		
	}

	private void performSheetValidation(List<SpreadSheet> sheets) {

		// Validate sheet names match exactly
		for(SpreadSheet sheet : sheets) {
			XSSFSheet xssfSheet = this.workbook.getSheet(sheet.getName());
			if(xssfSheet == null) {
				throw new RuntimeException("Workbook is unable to find sheet with name: " + sheet.getName());
			}
		}
	}
	
	private void applySheet(SpreadSheet sheet) {
		// Get the XSSFSheet from the name
		XSSFSheet xssfSheet = this.workbook.getSheet(sheet.getName());
		
		// For each cell in the spreadsheet, find the cell in the template and 
		// write to it
		for(Cell cell : sheet.getCells()) {
			Row row = this.rowManager.getRow(xssfSheet, cell.getCellKey().getRow());
			ApacheCellUtility.createCell(this.workbook, row, cell);
		}
	}
	
	@Override
	public void load(File file) {

		InputStream inputStream = null;

		try {
			inputStream = new FileInputStream(file);
			this.workbook = (XSSFWorkbook) WorkbookFactory.create(inputStream);
		} catch (InvalidFormatException | IOException e) {
			throw new RuntimeException(e);
		}
		
		this.templateFile = file;
	}


}
