package com.galasti.excellib.implementation;

import java.io.File;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.galasti.excellib.entry.IWorkbookWriter;
import com.galasti.excellib.structure.Cell;
import com.galasti.excellib.structure.SpreadSheet;
import com.galasti.excellib.structure.WorkbookDocument;

/**
 * Writes a new Excel Xlsx type file from a {@link WorkbookDocument} object. 
 * A full file path must be specified before calling the write() interface
 * using setFilePath(). Once write() interface is called, the file will be
 * written to disk, and the implementation will return a {@link File} object
 * to that file.
 * @author pgalasti
 *
 */
public class XlsxWriter implements IWorkbookWriter {

	private XSSFWorkbook workbook;
	private RowManager rowManager;
	
	private String filePath;
	
	/**
	 * Creates a new XlsxWriter object.
	 */
	public XlsxWriter() {
		this.rowManager = new RowManager();
	}
	
	@Override
	public File write(WorkbookDocument workbookDocument) {
		
		// Make sure the file path we set isn't empty.
		if(filePath.isEmpty()) {
			throw new RuntimeException("Unable to write xlsx. Need to call setFilePath first.");
		}
		
		// Create a new Apache XSSFWorkbook
		this.workbook = new XSSFWorkbook();
		
		// For each sheet in the workbook, create a new XSSFSheet and populate 
		// it with cell data.
		for(SpreadSheet sheet : workbookDocument.getSheets()) {
			XSSFSheet xssfSheet = this.workbook.createSheet(sheet.getName());
			this.writeSheetToDocument(xssfSheet, sheet);
		}
		
		return writeToFile();
	}
	
	/**
	 * Sets the file path for the new XLSX Excel file.
	 * @param filePath File path for the new XLSX Excel file. 
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	private File writeToFile() {
		
		try {
			
			// Get the file output stream to the path
			FileOutputStream fileOut = new FileOutputStream(this.filePath);
			
			// Call the Apache writing
			this.workbook.write(fileOut);
			
		} catch(Exception e) {
			throw new RuntimeException("Unable to write new XLSX file!", e);
		}
		
		// Return a File object of the newly created file
		return new File(this.filePath);
	}
	
	private void writeSheetToDocument(XSSFSheet xssfSheet, SpreadSheet sheet) {
		
		// For each cell found in the spreadsheet object, get the corresponding
		// XSSFSheet row and create the cell. 
		for(Cell cell : sheet.getCells()) {
			Row row = this.getRow(xssfSheet, cell.getCellKey().getRow());
			ApacheCellUtility.createCell(this.workbook, row, cell);
		}
		
	}
	
	private Row getRow(XSSFSheet xssfSheet, int rowNumber) {
		// Make sure the row number isn't 0
		if(rowNumber < 1) {
			throw new RuntimeException("Getting row failed. Row must be specified greater than 0!");
		}
		
		// Let the RowManager manage if it needs to create a new row or if it 
		// already has one to return
		return this.rowManager.getRow(xssfSheet, rowNumber-1);
	}
	
	//Moved to CellWriter utility to be reused
//	private org.apache.poi.ss.usermodel.Cell createCell(Row row, Cell cell) {
//		org.apache.poi.ss.usermodel.Cell apacheCell = row.createCell(cell.getColumnNumber());
//		
//		// Do data type creation
//		DataType cellDataType = cell.getFormat().getDataType();
//		switch(cellDataType) {
//		
//		case WholeNumber:
//			apacheCell.setCellType(org.apache.poi.ss.usermodel.Cell.CELL_TYPE_NUMERIC);
//			apacheCell.setCellValue((int)Double.parseDouble(cell.getData()));
//			break;
//			
//		case Decimal:
//			apacheCell.setCellType(org.apache.poi.ss.usermodel.Cell.CELL_TYPE_NUMERIC);
//			apacheCell.setCellValue(Double.parseDouble(cell.getData()));
//			break;
//			
//		case Text:
//			apacheCell.setCellType(org.apache.poi.ss.usermodel.Cell.CELL_TYPE_STRING);
//			apacheCell.setCellValue(cell.getData());
//			break;
//		}
//	
//		// Do formating: text color, background color, text type
//		XSSFCellStyle cellStyle = this.workbook.createCellStyle();
//		
//		XSSFFont textFont = this.workbook.createFont();
//		final Format cellFormat = cell.getFormat();
//		
//		// Do text type
//		final TextType textType = cellFormat.getTextType();
//		textFont.setItalic(textType.isItalic());
//		textFont.setBold(textType.isBold());
//		if(textType.isUnderline()) {
//			textFont.setUnderline(Font.U_SINGLE);
//		}
//		
//		// Do font type & size
//		final CellFont cellFont = cellFormat.getFont();
//		textFont.setFontHeightInPoints((short)cellFont.getSize());
//		textFont.setFontName(cellFont.getFont());
//		
//		
//		// Do text color
//		final Color textColor = cell.getFormat().getTextColor();
//		textFont.setColor(new XSSFColor(new java.awt.Color(textColor.getRed(), textColor.getGreen(), textColor.getBlue())));
//		cellStyle.setFont(textFont);
//		
//		// Do background color
//		final Color backgroundColor = cell.getFormat().getBackgroundColor();
//		if(!backgroundColor.isBlack()) {
//			cellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
//			cellStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(backgroundColor.getRed(), backgroundColor.getGreen(), backgroundColor.getBlue())));
//		}
//		
//		apacheCell.setCellStyle(cellStyle);
//
//		return apacheCell;
//	}
}
