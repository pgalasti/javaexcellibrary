package com.galasti.excellib.implementation;

import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;

/**
 * Manages whether a new row will need to be created or if it already stored
 * a created row.
 * @author pgalasti
 *
 */
public class RowManager {

	private Map<String,Map<Integer, Row>> createdRows;
	
	/**
	 * Creates a new RowManager object.
	 */
	public RowManager() {
		this.createdRows = new HashMap<>();
	}
	
	/**
	 * Returns a {@link Row} object and determines if it needs to create new
	 * Row object or if it had it stored previously.
	 * @param xssfSheet A {@link XSSFSheet} to extract a row from.
	 * @param rowNumber The row number to grab
	 * @return A previously or newly created {@link Row} object
	 */
	public Row getRow(XSSFSheet xssfSheet, int rowNumber) {
		
		final String sheetNameKey = xssfSheet.getSheetName().toLowerCase();
		Map<Integer, Row> sheetMap = this.createdRows.get(sheetNameKey);
		
		if(sheetMap == null) {
			Map<Integer, Row> newSheetMap = new HashMap<>();
			
			Row row = xssfSheet.getRow(rowNumber);
			if(row == null) {
				row = xssfSheet.createRow(rowNumber);
			}
			newSheetMap.put(rowNumber, row);
			this.createdRows.put(sheetNameKey, newSheetMap);
			return row;
		}
		
		Row row = sheetMap.get(rowNumber);
		if(row == null) {
			row = xssfSheet.createRow(rowNumber);
			sheetMap.put(rowNumber, row);
			return row;
		}
		
		return row;
	}
	
	/**
	 * Clears our the managed row information
	 */
	public void clear() {
		this.createdRows.values().clear();
		this.createdRows.clear();
	}
	
}
