package com.galasti.excellib.implementation;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.galasti.excellib.entry.IWorkbookReader;
import com.galasti.excellib.structure.Cell;
import com.galasti.excellib.structure.SpreadSheet;
import com.galasti.excellib.structure.WorkbookDocument;

/**
 * Reads an existing Excel Xlsx type file from a {@link WorkbookDocument} 
 * object.
 * @author pgalasti
 *
 */
public class XlsxReader implements IWorkbookReader {

	private XSSFWorkbook workbook;
	
	@Override
	public WorkbookDocument read(File file) {
		
		// Make sure we're reading XLSX type
		if(!this.validateXlsx(file.getName())) {
			throw new RuntimeException("File given to reader is not xlsx extension.");
		}
		
		InputStream inputStream = null;
		
		try {
			// Open an input stream to the xlsx file and create a XSSFWorkbook
			// object.
			inputStream = new FileInputStream(file);
			this.workbook = (XSSFWorkbook) WorkbookFactory.create(inputStream);
		} catch (InvalidFormatException | IOException e) {
			throw new RuntimeException(e);
		}
		
		WorkbookDocument workbookDocument = new WorkbookDocument();
		
		// For each sheet in the workbook, extract it's sheet information and
		// put it on our workbookDocument object4.
		final int numberOfSheets = this.workbook.getNumberOfSheets();
		for(int i = 0; i < numberOfSheets; i++) {
			
			// Get the sheet
			XSSFSheet xssfSheet = this.workbook.getSheetAt(i);
			
			// Extract information from the XSSFSheet into Spreadsheet
			SpreadSheet sheet = extractXssfSheet(xssfSheet);
			
			// Add it to our returning workbookDocument
			workbookDocument.addSheet(sheet);
		}
		
		return workbookDocument;
	}
	
	private boolean validateXlsx(String fileName) {
		
		// TODO Are there other types aside form xlsx?
		return true;
	}
	
	private SpreadSheet extractXssfSheet(XSSFSheet xssfSheet) {
		// Get the number of populated rows in the excel sheet
		final int rowCount = xssfSheet.getLastRowNum()+1;
		
		// Get the name from the XSSFSheet object
		SpreadSheet sheet = new SpreadSheet(xssfSheet.getSheetName());
		
		List<org.apache.poi.ss.usermodel.Cell> apacheCellList = null;
		for(int i = 0; i < rowCount; i++) {
			
			// Get all of the valid rows in the sheet
			Row row = xssfSheet.getRow(i);
			if(row == null) {
				continue;
			}

			// Get all of the valid cells in the row
			apacheCellList = this.getApacheCells(row);
			for(org.apache.poi.ss.usermodel.Cell cell : apacheCellList) {

				// Convert the apache implementation over to ours
				Cell newCell = ApacheCellUtility.convertApacheCellToWrappedCell(cell);
				
				// Add it to our returning spreadsheet
				sheet.addCell(newCell);
			}
		}
		
		return sheet;
	}
	
	private List<org.apache.poi.ss.usermodel.Cell> getApacheCells(Row row) {
		final int lastCellNumber = row.getLastCellNum();
		
		List<org.apache.poi.ss.usermodel.Cell> apacheCellList = new ArrayList<>();
		
		for(int i = 0; i < lastCellNumber; i++) {
			if(row.getCell(i) != null) {
				apacheCellList.add(row.getCell(i));
			}
		}
		
		return apacheCellList;
	}
	
	// Moved to ApacheCellUtility
//	private Cell convertApacheCellToWrappedCell(org.apache.poi.ss.usermodel.Cell apacheCell) {
//
//		Cell cell = new Cell();
//		
//		// Create the CellKey
//		int debug = apacheCell.getColumnIndex();
//		debug = apacheCell.getRowIndex()+1;
//		CellKey key = new CellKey(apacheCell.getColumnIndex(), apacheCell.getRowIndex()+1);
//		cell.setKey(key);
//		
//		
//		Format cellFormat = new Format();
//		
//		// Set the data and format
//		switch(apacheCell.getCellType()) {
//		
//		case org.apache.poi.ss.usermodel.Cell.CELL_TYPE_STRING:
//			cell.setData(apacheCell.getStringCellValue());
//			cellFormat.setDataType(DataType.Text);
//			break;
//			
//		case org.apache.poi.ss.usermodel.Cell.CELL_TYPE_NUMERIC:
//			final Double value = apacheCell.getNumericCellValue();
//			if(value % 1 == 0) { // Is a whole number
//				cell.setData(Integer.toString(value.intValue()));
//				cellFormat.setDataType(DataType.WholeNumber);
//			} else {
//				cell.setData(value.toString());
//				cellFormat.setDataType(DataType.Decimal);
//			}
//			break;
//			
//		case org.apache.poi.ss.usermodel.Cell.CELL_TYPE_BLANK:
//			String testDebug = apacheCell.getStringCellValue();
//			break;
//			
//		default:
//			throw new RuntimeException("Unable to convert Apache cell to cell! Unknown data type.");
//		}
//		
//		
//		XSSFCellStyle style = (XSSFCellStyle) apacheCell.getCellStyle();
//
//		// Get background color
//		if(style.getFillPattern() == XSSFCellStyle.SOLID_FOREGROUND) {
//			XSSFColor color = style.getFillForegroundColorColor();
//			byte[] rgbBytes = color.getRgb();
//			cellFormat.setBackgroundColor(new Color(rgbBytes[0], rgbBytes[1], rgbBytes[2]));
//		}
//		
//		// Get text color 
//		XSSFFont font =  style.getFont();
//		XSSFColor xssfColor = font.getXSSFColor();
//		byte[] rgbBytes = xssfColor.getRgb();
//		cellFormat.setTextColor(new Color(rgbBytes[0], rgbBytes[1], rgbBytes[2]));
//		
//		// Get text formatting
//		TextType textType = new TextType();
//		if(font.getBold()) {
//			textType.setBold();
//		}
//		if(font.getItalic()) {
//			textType.setItalic();
//		}
//		if(font.getUnderline() == Font.U_SINGLE) {
//			textType.setUnderline();
//		}
//		cellFormat.setTextType(textType);
//		
//		// Get font
//		final String fontName = font.getFontName();
//		final long fontSize = font.getFontHeightInPoints();
//		cellFormat.setFont(new CellFont(fontName, fontSize));
//		
//		// Apply the format
//		cell.setFormat(cellFormat);
//		
//		return cell;
//	}
}
