package com.galasti.excellib.implementation;

import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;

import com.galasti.excellib.structure.Cell;
import com.galasti.excellib.structure.CellFont;
import com.galasti.excellib.structure.CellKey;
import com.galasti.excellib.structure.Color;
import com.galasti.excellib.structure.DataType;
import com.galasti.excellib.structure.Format;
import com.galasti.excellib.structure.TextType;

/**
 * Utility class to manage conversion between cell information using Apache POI 
 * library.
 * @author pgalasti
 *
 */
public class ApacheCellUtility {

	/**
	 * Creates a {@link org.apache.poi.ss.usermodel.Cell} object from a 
	 * {@link Workbook} object.
	 * @param workbook A {@link Workbook} object 
	 * @param row A {@link Row} object the cell will be created on
	 * @param cell {@link Cell} object passed to be created.
	 * @return A newly created {@link org.apache.poi.ss.usermodel.Cell} object
	 */
	public static org.apache.poi.ss.usermodel.Cell createCell(Workbook workbook, Row row, Cell cell) {

		// Create the cell for that row.
		org.apache.poi.ss.usermodel.Cell apacheCell = row.createCell(cell.getColumnNumber());

		// Do data type creation
		DataType cellDataType = cell.getFormat().getDataType();
		switch(cellDataType) {

		case WholeNumber:
			apacheCell.setCellType(org.apache.poi.ss.usermodel.Cell.CELL_TYPE_NUMERIC);
			apacheCell.setCellValue((int)Double.parseDouble(cell.getData()));
			break;

		case Decimal:
			apacheCell.setCellType(org.apache.poi.ss.usermodel.Cell.CELL_TYPE_NUMERIC);
			apacheCell.setCellValue(Double.parseDouble(cell.getData()));
			break;

		case Text:
			apacheCell.setCellType(org.apache.poi.ss.usermodel.Cell.CELL_TYPE_STRING);
			apacheCell.setCellValue(cell.getData());
			break;
		}

		// Do formating: text color, background color, text type
		XSSFCellStyle cellStyle = (XSSFCellStyle) workbook.createCellStyle();

		XSSFFont textFont = (XSSFFont) workbook.createFont();
		final Format cellFormat = cell.getFormat();

		// Do text type
		final TextType textType = cellFormat.getTextType();
		textFont.setItalic(textType.isItalic());
		textFont.setBold(textType.isBold());
		if(textType.isUnderline()) {
			textFont.setUnderline(Font.U_SINGLE);
		}

		// Do font type & size
		final CellFont cellFont = cellFormat.getFont();
		textFont.setFontHeightInPoints((short)cellFont.getSize());
		textFont.setFontName(cellFont.getFont());


		// Do text color
		final Color textColor = cell.getFormat().getTextColor();
		textFont.setColor(new XSSFColor(new java.awt.Color(textColor.getRed(), textColor.getGreen(), textColor.getBlue())));
		cellStyle.setFont(textFont);

		// Do background color
		final Color backgroundColor = cell.getFormat().getBackgroundColor();
		if(!backgroundColor.isBlack()) {
			cellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			cellStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(backgroundColor.getRed(), backgroundColor.getGreen(), backgroundColor.getBlue())));
		}

		// Set the new style to the Apache cell
		apacheCell.setCellStyle(cellStyle);

		return apacheCell;
	}
	
	public static Cell convertApacheCellToWrappedCell(org.apache.poi.ss.usermodel.Cell apacheCell) {

		Cell cell = new Cell();
		
		// Create the CellKey
		int debug = apacheCell.getColumnIndex();
		debug = apacheCell.getRowIndex()+1;
		CellKey key = new CellKey(apacheCell.getColumnIndex(), apacheCell.getRowIndex()+1);
		cell.setKey(key);
		
		
		Format cellFormat = new Format();
		
		// Set the data and format
		switch(apacheCell.getCellType()) {
		
		case org.apache.poi.ss.usermodel.Cell.CELL_TYPE_STRING:
			cell.setData(apacheCell.getStringCellValue());
			cellFormat.setDataType(DataType.Text);
			break;
			
		case org.apache.poi.ss.usermodel.Cell.CELL_TYPE_NUMERIC:
			final Double value = apacheCell.getNumericCellValue();
			if(value % 1 == 0) { // Is a whole number
				cell.setData(Integer.toString(value.intValue()));
				cellFormat.setDataType(DataType.WholeNumber);
			} else {
				cell.setData(value.toString());
				cellFormat.setDataType(DataType.Decimal);
			}
			break;
		
		case org.apache.poi.ss.usermodel.Cell.CELL_TYPE_FORMULA: // I don't think we'll need to care about forumla information
		case org.apache.poi.ss.usermodel.Cell.CELL_TYPE_BLANK:
			break;
			
		default:
			throw new RuntimeException("Unable to convert Apache cell to cell! Unknown data type.");
		}
		
		
		XSSFCellStyle style = (XSSFCellStyle) apacheCell.getCellStyle();

		// Get background color
		if(style.getFillPattern() == XSSFCellStyle.SOLID_FOREGROUND) {
			XSSFColor color = style.getFillForegroundColorColor();
			byte[] rgbBytes = color.getRgb();
			cellFormat.setBackgroundColor(new Color(rgbBytes[0], rgbBytes[1], rgbBytes[2]));
		}
		
		// Get text color 
		XSSFFont font =  style.getFont();
		XSSFColor xssfColor = font.getXSSFColor();
		byte[] rgbBytes = xssfColor.getRgb();
		cellFormat.setTextColor(new Color(rgbBytes[0], rgbBytes[1], rgbBytes[2]));
		
		// Get text formatting
		TextType textType = new TextType();
		if(font.getBold()) {
			textType.setBold();
		}
		if(font.getItalic()) {
			textType.setItalic();
		}
		if(font.getUnderline() == Font.U_SINGLE) {
			textType.setUnderline();
		}
		cellFormat.setTextType(textType);
		
		// Get font
		final String fontName = font.getFontName();
		final long fontSize = font.getFontHeightInPoints();
		cellFormat.setFont(new CellFont(fontName, fontSize));
		
		// Apply the format
		cell.setFormat(cellFormat);
		
		return cell;
	}

}
