package com.galasti.excellib.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.galasti.excellib.implementation.XlsxTemplateWriter;
import com.galasti.excellib.structure.Cell;
import com.galasti.excellib.structure.CellKey;
import com.galasti.excellib.structure.SpreadSheet;


public class Test {

	public static void main(String[] args) {
		
//		XlsxWriter writer = new XlsxWriter();
//		writer.setFilePath("/home/pgalasti/workbook_testing.xlsx");
//		
//		ExcelDocument document = new ExcelDocument("testabc.xlsx");
//		ExcelSheet sheet = new ExcelSheet("TestSheet");
//		ExcelSheet sheet2 = new ExcelSheet("TestSheet2");
//		document.addSheet(sheet);
//		document.addSheet(sheet2);
//		
//		Cell testCell = new Cell();
//		testCell.setKey("B", 1);
//		testCell.setFormat(new Format().setDataType(DataType.Decimal).setTextType(new TextType().setBold().setItalic().setUnderline()));
//		testCell.setData("1.5");
//		sheet.addCell(testCell);
//		
//		Cell testCell2 = new Cell();
//		testCell2.setKey("B", 2);
//		testCell2.setFormat(new Format().setDataType(DataType.Text).setTextColor(new Color((byte)255, (byte)0, (byte)0)));
//		testCell2.setData("1.5");
//		
//		
//		sheet.addCell(testCell2);
//		
//		Cell testCell3 = new Cell();
//		testCell3.setKey("B", 3);
//		testCell3.setFormat(new Format().setDataType(DataType.WholeNumber).setBackgroundColor(new Color((byte)0, (byte)255, (byte)0)));
//		testCell3.setData("1.5");
//		sheet.addCell(testCell3);
//		
//		Cell farCell = new Cell();
//		farCell.setKey("BC", 10);
//		Format form = new Format();
//		form.setFont(new CellFont("Arial", 30));
//		farCell.setFormat(form);
//		farCell.setData("BC");
//		sheet.addCell(farCell);
//		
//		Cell farCell2 = new Cell();
//		farCell2.setKey("BAA", 10);
//		farCell2.setData("BAA");
//		sheet.addCell(farCell2);
//		
//		Cell farCell3 = new Cell();
//		farCell3.setKey("CD", 10);
//		farCell3.setData("CD");
//		sheet.addCell(farCell3);
//		
//		Cell farCell4 = new Cell();
//		farCell4.setKey("AA", 10);
//		farCell4.setData("AA");
//		sheet.addCell(farCell4);
//		
//		writer.write(document);
//		System.out.println("Finished");
//
//		
//		String debug = CellKey.getNumericColumnString(2698);
//		XlsxReader reader = new XlsxReader();
//		ExcelDocument document = reader.read(new File("/home/pgalasti/workbook_testing.xlsx"));
//		
//		XlsxWriter writer = new XlsxWriter();
//		writer.setFilePath("/home/pgalasti/workbook_testing_output.xlsx");
//		writer.write(document);
//		
//		XlsxReader reader = new XlsxReader();
//		reader.read(new File("/home/pgalasti/testValidations.xlsx"));
		
		XlsxTemplateWriter overwriter = new XlsxTemplateWriter();
		overwriter.load(new File("/home/pgalasti/template.xlsx"));
		
		SpreadSheet sheet = new SpreadSheet("TestSheet");
		Cell cellD4 = new Cell(new CellKey("D", 1));
		cellD4.setData("Testing D4");
		
		sheet.addCell(cellD4);

		List<SpreadSheet> sheets = new ArrayList<>();
		sheets.add(sheet);
		
		overwriter.put(sheets);
		System.out.println("Finished");
	}
	
}
