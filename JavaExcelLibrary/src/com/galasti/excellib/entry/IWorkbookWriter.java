package com.galasti.excellib.entry;

import java.io.File;

import com.galasti.excellib.structure.WorkbookDocument;

/**
 * An interface to write a new workbook document from memory to the file system.
 * @author pgalasti
 *
 */
public interface IWorkbookWriter {

	/**
	 * An interface to write a {@link WorkbookDocument} to a {@link File} 
	 * object.
	 * @param workbookDocument A {@link WorkbookDocument} to be writtento file
	 * @return A {@link File}
	 */
	public File write(WorkbookDocument workbookDocument);
}
