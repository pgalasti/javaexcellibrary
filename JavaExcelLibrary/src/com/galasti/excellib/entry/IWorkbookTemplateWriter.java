package com.galasti.excellib.entry;

import java.io.File;
import java.util.List;

import com.galasti.excellib.structure.SpreadSheet;

public interface IWorkbookTemplateWriter {

	public void put(List<SpreadSheet> sheets);
	public void load(File file);
	
}
