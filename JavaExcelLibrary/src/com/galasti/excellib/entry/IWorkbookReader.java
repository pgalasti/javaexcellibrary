package com.galasti.excellib.entry;

import java.io.File;

import com.galasti.excellib.structure.WorkbookDocument;

/**
 * An interface to read an existing workbook document from the file system into memory.
 * @author pgalasti
 *
 */
public interface IWorkbookReader {

	/**
	 * An interface to read a {@link File} to a {@link WorkbookDocument} object
	 * @param file File of a Workbook file
	 * @return A {@link WorkbookDocument} object
	 */
	public WorkbookDocument read(File file);
}
